Feature: import agreements

  Background:
    * url 'http://' + host  + ':8081'

  Scenario: import agreements and delete them

    * def body = read('bulk-agreements.json')

    Given path '/agreements/bulk/'
    And request body
    And header Authorization = jwtAdminToken
    When method post
    Then status 201
    And def created = response

    * assert created.length == 2
    * match created[*].code contains [ 'AAA', 'BBB']
    * def result2 = call read('delete-agreement.feature') created