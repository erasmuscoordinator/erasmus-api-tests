@ignore
Feature: create new agreement

  Background:
    * url 'http://' + host  + ':8081'

  Scenario: create new agreement

    * def body =
      """
      {
        "code": '#(code)',
        "coordinator": '#(coordinator)',
        "country": '#(country)',
        "department": '#(department)',
        "duration": '#(duration)',
        "endYear": '#(endYear)',
        "erasmusCoordinator": '#(erasmusCoordinator)',
        "startYear": '#(startYear)',
        "universityName": '#(universityName)',
        "vacancies": '#(vacancies)'
      }
      """

    Given path '/agreements/'
    And request body
    And header Authorization = jwtAdminToken
    When method post
    Then status 201