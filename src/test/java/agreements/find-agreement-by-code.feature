Feature: find agreements by code

  Background:
    * url 'http://' + host  + ':8081'

  Scenario: create agreements, search them by code and delete them

    * def callable1 = call read('create-agreement.feature') read('agreement1.json')
    * def callable2 = call read('create-agreement.feature') read('agreement2.json')
    * def agreement1 = callable1.response
    * def agreement2 = callable2.response

    Given path '/agreements/search/codes'
    And params { code: 'AAA'}
    And header Authorization = jwtAdminToken
    When method get
    Then status 200
    And def found = response

    * assert found.page.totalElements == 1
    * match found._embedded.agreements[*].code contains [ 'AAA' ]
    * def result2 = call read('delete-agreement.feature') found._embedded.agreements

    Given path '/agreements/search/codes'
    And params { code: 'BBB'}
    And header Authorization = jwtAdminToken
    When method get
    Then status 200
    And def found = response

    * assert found.page.totalElements == 1
    * match found._embedded.agreements[*].code contains [ 'BBB' ]
    * def result2 = call read('delete-agreement.feature') found._embedded.agreements