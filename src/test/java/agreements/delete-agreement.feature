@ignore
Feature: delete agreement

  Background:
    * url 'http://' + host  + ':8081'

  Scenario: delete agreement

    # FIXME fix status code for agreement delete to 200
    Given path '/agreements', id
    And header Authorization = jwtAdminToken
    When method delete
    Then status 204