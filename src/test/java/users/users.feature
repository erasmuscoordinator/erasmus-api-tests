Feature: test user creation and deletion

  Background:
    * url 'http://' + host  + ':8082'

  Scenario: register user, get it by id and delete

    * def callable = call read('create-user.feature') read('user1.json')
    * def user = callable.response
    * print 'created id is: ' + user.id

    Given path '/login'
    And request { email: '#(user.email)', password: 'password' }
    And header Accept = 'application/json'
    When method POST
    Then status 200
    And def authToken = responseHeaders['Authorization'][0]

    Given path '/api/users/', user.id
    And header Authorization = authToken
    When method get
    Then status 200
    And match response contains user

    Given path '/api/users/', user.id
    And header Authorization = authToken
    When method delete
    Then status 200
    And match response.id contains user.id
    And match response contains user