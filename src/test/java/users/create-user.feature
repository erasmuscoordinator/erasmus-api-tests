@ignore
Feature: create new user

  Background:
    * url 'http://' + host  + ':8082'

  Scenario: create user

    * def body =
      """
      {
          "email": '#(email)',
          "firstName": '#(firstName)',
          "lastName": '#(lastName)',
          "password": '#(password)'
      }
      """

    Given path '/api/users/'
    And request body
    When method post
    Then status 201