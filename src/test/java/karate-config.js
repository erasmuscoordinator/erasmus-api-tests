function() {    
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
	myVarName: 'someValue',
    jwtAdminToken: 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkB0ZXN0LnBsIiwicm9sZSI6IkFETUlOSVNUUkFUT1IiLCJleHAiOjE1MzkxMjMwMjB9.eqRORLeOQhS6MRdEAs6oSO25O51HJNI1UHPiH1ytkBa6vJaJKP05JAdrQaZLCsVwMnvZR8oldTmIn5VUN5AZpw',
    jwtStudentToken: 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzdHVkZW50QHRlc3QucGwiLCJyb2xlIjoiU1RVREVOVCIsImV4cCI6MTUzOTEyMjk1Nn0.9S6SWGYYt7nExZOZ3StzmbpdrzDT28sojOPh_Uzflp4LmHfndc3AMznTKs6OQ7K1XSQM401DPahL4q6QcaQ1-g',
    host: '217.182.79.79'
  }
  if (env == 'dev') {
    // customize
    // e.g. config.foo = 'bar';1
  } else if (env == 'e2e') {
    // customize
  }
  return config;
}